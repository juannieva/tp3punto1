package tp3;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.math.BigDecimal;
import java.time.Duration;

public class Turno {
    private float horasTrabajadas;
    private LocalDateTime fechayHoraEgreso;
    private LocalDateTime fechayHoraIngreso;

    public Turno(){}

    public Turno(LocalDateTime fechaIngreso , LocalDateTime fechaEgreso){
        this.fechayHoraIngreso = fechaIngreso;
        this.fechayHoraEgreso = fechaEgreso;
        calculoHoras();
    }
    public void calculoHoras(){
        horasTrabajadas= Duration.between(fechayHoraIngreso,fechayHoraEgreso).toHours();
    
    }
    public float getHorasTrabajadas(){
        return horasTrabajadas;
    }
    
}