package tp3;

import java.util.ArrayList;


public class Empleado {
   private String nombre;
   private String apellido;
   private Integer dni;
   private float costoxHoraTrabajada;
   private ArrayList<Turno> turnos;
   private float sueldoTotal;
   
   public Empleado(String nombre, String apellido,Integer dni, float costoxHoraTrabajada) {
    this.nombre = nombre;
    this.apellido = apellido;
    this.dni = dni;
    this.costoxHoraTrabajada = costoxHoraTrabajada;
    setTurnos();
    }

   public void setTurnos(){
    turnos = new ArrayList<Turno>();
   }

   public String getNombre() {
       return nombre;
   }

   public void setNombre(String nombre) {
       this.nombre = nombre;
   }

   public String getApellido() {
       return apellido;
   }

   public void setApellido(String apellido) {
       this.apellido = apellido;
   }

   public float getCostoxHoraTrabajada() {
       return costoxHoraTrabajada;
   }

   public void setCostoxHoraTrabajada(float costoxHoraTrabajada) {
       this.costoxHoraTrabajada = costoxHoraTrabajada;
   }

   public void agregarTurnos(Turno turno){
       this.turnos.add(turno);
       setSueldoTotal();
   }

   public Turno getTurnos(int e){
       Turno aux= new Turno();
        for (int i=0;i<turnos.size();i++) {
        if(e==i){
            aux=turnos.get(i);
            break;
        }
    }
        return aux;
   }

    public float getSueldoTotal() {
    return sueldoTotal;
}

    private void setSueldoTotal() {
        sueldoTotal=0;
        for (Turno temp: turnos) {
            float sueldoParcial;
            sueldoParcial= costoxHoraTrabajada * temp.getHorasTrabajadas();
            this.sueldoTotal= sueldoTotal + sueldoParcial;
        }
    }

public Integer getDni() {
	return dni;
}

public void setDni(Integer dni) {
	this.dni = dni;
}

   
}