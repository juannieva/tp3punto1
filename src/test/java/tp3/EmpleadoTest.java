package tp3;

import org.junit.Test;
import static org.junit.Assert.*;

import java.time.LocalDateTime;

public class EmpleadoTest {

    @Test public void agregarEmpleadoyCalcularunTurno(){
        LocalDateTime fechaEntrada= LocalDateTime.parse("2020-10-01T10:00:00");
        LocalDateTime fechaSalida= LocalDateTime.parse("2020-10-01T13:00:00");   
        Turno turno = new Turno (fechaEntrada,fechaSalida);
        Empleado empleado1 = new Empleado("Juan","Pablo",40814273,200);
        empleado1.agregarTurnos(turno);
        assertEquals("Juan", empleado1.getNombre());
        assertEquals("Pablo", empleado1.getApellido());
        assertEquals(600 ,empleado1.getSueldoTotal() ,0);
    }
    @Test public void agregarEmpleadoyCalcular2Turno(){
        LocalDateTime fechaEntrada= LocalDateTime.parse("2020-10-01T10:00:00");
        LocalDateTime fechaSalida= LocalDateTime.parse("2020-10-01T13:00:00"); 
        Turno turno = new Turno (fechaEntrada,fechaSalida);
        fechaEntrada=LocalDateTime.parse("2020-10-02T10:00:00");
        fechaSalida= LocalDateTime.parse("2020-10-02T13:00:00");
        Turno turno1 = new Turno (fechaEntrada,fechaSalida);
        Empleado empleado1 = new Empleado("Juan","Pablo",40814273,200);
        empleado1.agregarTurnos(turno);
        empleado1.agregarTurnos(turno1);
        assertEquals("Juan", empleado1.getNombre());
        assertEquals("Pablo", empleado1.getApellido());
        assertEquals(1200, empleado1.getSueldoTotal(),0);

    }
}
