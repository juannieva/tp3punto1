package tp3;

import org.junit.Test;
import static org.junit.Assert.*;
import java.time.LocalDateTime;


public class TurnoTest {

    @Test public void calcularHoras() {
        LocalDateTime fechaEntrada= LocalDateTime.parse("2020-10-01T10:00:00");
        LocalDateTime fechaSalida= LocalDateTime.parse("2020-10-01T14:00:00");  
        
        Turno turno = new Turno (fechaEntrada,fechaSalida);

        assertEquals(4 ,turno.getHorasTrabajadas() ,0);
    }
    
}
